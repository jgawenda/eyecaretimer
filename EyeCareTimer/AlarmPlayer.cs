﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.IO;

namespace EyeCareTimer
{
    class AlarmPlayer
    {
        public static void Play(string filename)
        {
            if (File.Exists(filename))
            {
                SoundPlayer player = new SoundPlayer(filename);
                player.Play();
            }

        }
    }
}
