﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EyeCareTimer
{
    public partial class EyeCare : Form, IMainForm
    {
        private TimeManager _timeManager;
        private TimeUpForm _timeUpForm;

        public Label MinutesLabel
        {
            get
            {
                return labelShowMinutes;
            }
        }

        public EyeCare()
        {
            InitializeComponent();
        }

        private void EyeCare_Load(object sender, EventArgs e)
        {
            _timeManager = new TimeManager(this);

            _timeUpForm = new TimeUpForm(this);

        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            
            if (buttonStart.Text == "Start")
            {
                int miliseconds = ConvertStringToSeconds(textBoxMinutes);

                if (miliseconds > 0)
                {
                    textBoxMinutes.BackColor = Color.White;
                    buttonStart.Text = "Stop";
                    textBoxMinutes.Enabled = false;
                    _timeManager.SetInterval(miliseconds);
                    _timeManager.Start();
                    timerCheckIfTimeEnded.Start();
                }

            }
            else
            {
                ResetTheControls();
                _timeManager.Stop();
                timerCheckIfTimeEnded.Stop();
                labelShowMinutes.Text = "00:00";
            }
            

        }

        private int ConvertStringToSeconds(TextBox textBox)
        {
            int outputMiliseconds = 0;

            if (!Int32.TryParse(textBox.Text, out outputMiliseconds))
                textBox.BackColor = Color.OrangeRed;

            outputMiliseconds = outputMiliseconds * 60000;

            return outputMiliseconds;
        }

        private void ResetTheControls()
        {
            buttonStart.Text = "Start";
            textBoxMinutes.Enabled = true;
        }

        delegate void SetTextCallback(string text, Label label);
        
        public void SetTextTime(string text, Label label)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (label.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetTextTime);
                Invoke(d, new object[] { text, label });
            }
            else
            {
                label.Text = text;
            }
        }

        private void TimerCheckIfTimeEnded_Tick(object sender, EventArgs e)
        {
            if (_timeManager.HasTimeEnded)
            {
                _timeUpForm.Show();
                _timeUpForm.Activate();
                notifyIcon.Visible = false;
                ResetTheControls();
                timerCheckIfTimeEnded.Stop();
                AlarmPlayer.Play("alarm.wav");

                //BringToFront();
                //ShowAndMaximizeWindow();
            }
        }
        
        private void ShowAndMaximizeWindow()
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon.Visible = false;
        }
        
        private void NotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowAndMaximizeWindow();
        }

        private void EyeCare_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized && buttonStart.Text == "Stop")
            {
                Hide();
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(2000);
            }

        }

    }
}
