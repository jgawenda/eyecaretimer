﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EyeCareTimer
{
    interface IMainForm
    {
        Label MinutesLabel { get; }
        void SetTextTime(string text, Label label);
    }
}
