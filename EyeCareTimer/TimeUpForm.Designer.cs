﻿namespace EyeCareTimer
{
    partial class TimeUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeUpForm));
            this.labelHeader = new System.Windows.Forms.Label();
            this.buttonDelay = new System.Windows.Forms.Button();
            this.buttonStartBreak = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.timerCheckIfTimeEnded = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // labelHeader
            // 
            this.labelHeader.AutoSize = true;
            this.labelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHeader.Location = new System.Drawing.Point(12, 9);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(248, 25);
            this.labelHeader.TabIndex = 0;
            this.labelHeader.Text = "Time\'s up. Take a break.";
            // 
            // buttonDelay
            // 
            this.buttonDelay.Location = new System.Drawing.Point(137, 84);
            this.buttonDelay.Name = "buttonDelay";
            this.buttonDelay.Size = new System.Drawing.Size(99, 36);
            this.buttonDelay.TabIndex = 2;
            this.buttonDelay.TabStop = false;
            this.buttonDelay.Text = "Delay 5 minutes";
            this.buttonDelay.UseVisualStyleBackColor = true;
            this.buttonDelay.Click += new System.EventHandler(this.ButtonDelay_Click);
            // 
            // buttonStartBreak
            // 
            this.buttonStartBreak.Location = new System.Drawing.Point(39, 84);
            this.buttonStartBreak.Name = "buttonStartBreak";
            this.buttonStartBreak.Size = new System.Drawing.Size(75, 36);
            this.buttonStartBreak.TabIndex = 1;
            this.buttonStartBreak.TabStop = false;
            this.buttonStartBreak.Text = "Start break";
            this.buttonStartBreak.UseVisualStyleBackColor = true;
            this.buttonStartBreak.Click += new System.EventHandler(this.ButtonStartBreak_Click);
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTime.Location = new System.Drawing.Point(79, 47);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(123, 25);
            this.labelTime.TabIndex = 3;
            this.labelTime.Text = "20 seconds";
            // 
            // timerCheckIfTimeEnded
            // 
            this.timerCheckIfTimeEnded.Tick += new System.EventHandler(this.TimerCheckIfTimeEnded_Tick);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipText = "You have 5 more minutes :)";
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Eye Care Timer";
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon_MouseDoubleClick);
            // 
            // TimeUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 136);
            this.ControlBox = false;
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.buttonStartBreak);
            this.Controls.Add(this.buttonDelay);
            this.Controls.Add(this.labelHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TimeUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Take a break";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TimeUpForm_FormClosed);
            this.VisibleChanged += new System.EventHandler(this.TimeUpForm_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHeader;
        private System.Windows.Forms.Button buttonDelay;
        private System.Windows.Forms.Button buttonStartBreak;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Timer timerCheckIfTimeEnded;
        private System.Windows.Forms.NotifyIcon notifyIcon;
    }
}