﻿namespace EyeCareTimer
{
    partial class EyeCare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EyeCare));
            this.textBoxMinutes = new System.Windows.Forms.TextBox();
            this.labelMinutes = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.labelShowMinutes = new System.Windows.Forms.Label();
            this.labelTimeLeft = new System.Windows.Forms.Label();
            this.timerCheckIfTimeEnded = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // textBoxMinutes
            // 
            this.textBoxMinutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxMinutes.Location = new System.Drawing.Point(24, 48);
            this.textBoxMinutes.Name = "textBoxMinutes";
            this.textBoxMinutes.Size = new System.Drawing.Size(67, 29);
            this.textBoxMinutes.TabIndex = 0;
            this.textBoxMinutes.Text = "20";
            this.textBoxMinutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelMinutes
            // 
            this.labelMinutes.AutoSize = true;
            this.labelMinutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMinutes.Location = new System.Drawing.Point(20, 21);
            this.labelMinutes.Name = "labelMinutes";
            this.labelMinutes.Size = new System.Drawing.Size(76, 24);
            this.labelMinutes.TabIndex = 1;
            this.labelMinutes.Text = "Minutes";
            // 
            // buttonStart
            // 
            this.buttonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonStart.Location = new System.Drawing.Point(127, 33);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(110, 35);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // labelShowMinutes
            // 
            this.labelShowMinutes.AutoSize = true;
            this.labelShowMinutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelShowMinutes.Location = new System.Drawing.Point(98, 110);
            this.labelShowMinutes.Name = "labelShowMinutes";
            this.labelShowMinutes.Size = new System.Drawing.Size(66, 25);
            this.labelShowMinutes.TabIndex = 3;
            this.labelShowMinutes.Text = "00:00";
            // 
            // labelTimeLeft
            // 
            this.labelTimeLeft.AutoSize = true;
            this.labelTimeLeft.Location = new System.Drawing.Point(96, 95);
            this.labelTimeLeft.Name = "labelTimeLeft";
            this.labelTimeLeft.Size = new System.Drawing.Size(71, 13);
            this.labelTimeLeft.TabIndex = 4;
            this.labelTimeLeft.Text = "Elapsed Time";
            // 
            // timerCheckIfTimeEnded
            // 
            this.timerCheckIfTimeEnded.Interval = 1000;
            this.timerCheckIfTimeEnded.Tick += new System.EventHandler(this.TimerCheckIfTimeEnded_Tick);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipText = "I will notify you when break comes :)";
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Eye Care Timer";
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon_MouseDoubleClick);
            // 
            // EyeCare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 152);
            this.Controls.Add(this.labelTimeLeft);
            this.Controls.Add(this.labelShowMinutes);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.labelMinutes);
            this.Controls.Add(this.textBoxMinutes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "EyeCare";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Eye Care Timer";
            this.Load += new System.EventHandler(this.EyeCare_Load);
            this.Resize += new System.EventHandler(this.EyeCare_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxMinutes;
        private System.Windows.Forms.Label labelMinutes;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelShowMinutes;
        private System.Windows.Forms.Label labelTimeLeft;
        private System.Windows.Forms.Timer timerCheckIfTimeEnded;
        private System.Windows.Forms.NotifyIcon notifyIcon;
    }
}

