﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Diagnostics;

namespace EyeCareTimer
{
    class TimeManager
    {
        private System.Timers.Timer _timer;
        private System.Timers.Timer _refreshElapsedTime;
        private IMainForm _mainForm;
        private DateTime _startTime;
        private TimeSpan _timeElapsed;

        public bool HasTimeEnded { get; private set; }

        public TimeManager(IMainForm mainForm)
        {
            _mainForm = mainForm;
            _timer = new System.Timers.Timer();
            _timer.AutoReset = false;
            _timer.Elapsed += Timer_TimeElapsed;

            _refreshElapsedTime = new Timer(1000);
            _refreshElapsedTime.Elapsed += RefreshElapsedTime_Elapsed;

            HasTimeEnded = false;
        }

        private void RefreshElapsedTime_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timeElapsed = DateTime.Now - _startTime;
            string timeToShow = String.Format("{0:00}:{1:00}", _timeElapsed.Minutes, _timeElapsed.Seconds);
            _mainForm.SetTextTime(timeToShow, _mainForm.MinutesLabel);
        }

        private void Timer_TimeElapsed(object sender, ElapsedEventArgs e)
        {
            _refreshElapsedTime.Stop();
            HasTimeEnded = true;

            if (_mainForm is EyeCare)
                _mainForm.SetTextTime("00:00", _mainForm.MinutesLabel);
            else
                _mainForm.SetTextTime("20 seconds", _mainForm.MinutesLabel);
        }
        
        public void SetInterval(int miliseconds)
        {
            _timer.Interval = miliseconds;
        }

        public void Start()
        {
            _startTime = DateTime.Now;
            HasTimeEnded = false;
            _timer.Start();
            _refreshElapsedTime.Start();
            
        }

        public void Stop()
        {
            _timer.Stop();
            _refreshElapsedTime.Stop();
        }
    }
}
