﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EyeCareTimer
{
    public partial class TimeUpForm : Form, IMainForm
    {
        private TimeManager _timeManager;
        private Form _parentForm;
        private List<Button> _allButtons;
        private bool _isDelayActive;

        public Label MinutesLabel
        {
            get
            {
                return labelTime;
            }
        }

        public TimeUpForm(Form parent)
        {
            InitializeComponent();
            _timeManager = new TimeManager(this);
            _parentForm = parent;
            _allButtons = new List<Button>() { buttonStartBreak, buttonDelay };
            _isDelayActive = false;
        }

        delegate void SetTextCallback(string text, Label label);

        public void SetTextTime(string text, Label label)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (label.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetTextTime);
                Invoke(d, new object[] { text, label });
            }
            else
            {
                label.Text = text;
            }
        }

        private void ButtonStartBreak_Click(object sender, EventArgs e)
        {
            DisableAllButtons();
            labelHeader.Text = "Look away for 20 seconds";
            _timeManager.SetInterval(20000);
            _timeManager.Start();
            timerCheckIfTimeEnded.Start();
        }

        private void TimerCheckIfTimeEnded_Tick(object sender, EventArgs e)
        {
            if (_timeManager.HasTimeEnded)
            {

                timerCheckIfTimeEnded.Stop();

                switch (_isDelayActive)
                {
                    case true:
                        AlarmPlayer.Play("alarm.wav");
                        ShowAndMaximizeWindow();
                        _isDelayActive = false;
                        break;
                    case false:
                        AlarmPlayer.Play("alarm1.wav");
                        Visible = false;
                        _parentForm.Show();
                        _parentForm.WindowState = FormWindowState.Normal;
                        break;
                }
                
            }
        }

        private void DisableAllButtons()
        {
            foreach (Button button in _allButtons)
            {
                button.Enabled = false;
            }
        }

        private void EnableAllButtons()
        {
            foreach (Button button in _allButtons)
            {
                button.Enabled = true;
            }
        }
        
        private void TimeUpForm_VisibleChanged(object sender, EventArgs e)
        {
            labelTime.Focus();
            labelHeader.Text = "Time's up. Take a break.";

            if (_parentForm.Visible == true)
            {
                _parentForm.WindowState = FormWindowState.Minimized;
                _parentForm.Visible = false;
            }

            if (_timeManager.HasTimeEnded)
                EnableAllButtons();
        }

        private void ButtonDelay_Click(object sender, EventArgs e)
        {
            DisableAllButtons();
            Hide();
            _isDelayActive = true;
            _timeManager.SetInterval(300000);
            _timeManager.Start();
            timerCheckIfTimeEnded.Start();

            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(2000);
        }

        private void NotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //ShowAndMaximizeWindow();
            notifyIcon.ShowBalloonTip(2000, "Delay", "Delay is active", ToolTipIcon.Info);
        }

        private void ShowAndMaximizeWindow()
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon.Visible = false;
        }

        private void TimeUpForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _parentForm.Close();
        }
    }
}
